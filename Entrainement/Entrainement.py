#!/usr/bin/env python
# -*- coding: utf-8 -*-

from CoocurrencesCalculator import CoocurrencesCalculator
from FichierConnection import FichierConnection
from GestionCMD import GestionCMD
import sys
import re
import numpy as np
from numpy import integer
import time
from itertools import zip_longest
import csv

#****************CONTROLEUR******************
def splitArgs():
    dictArg = {}
    e = True
    for i, value in enumerate(sys.argv):
        if(value == '-t'):
#             print("Taille de la fenetre :" , sys.argv[i+1])
            dictArg["t"] = int(sys.argv[i+1])
        elif (value == '-enc'):
#             print("Encodage : " , sys.argv[i+1])
            dictArg["enc"] = sys.argv[i+1]
        elif (value == '-cc'):
#             print("Chemin fichier : " , sys.argv[i+1])
            dictArg["cc"] = sys.argv[i+1]
        elif (value == '-e'):
#             print("ENTRAINEMENT")
            e = True
        elif (value == '-s'):
#             print("SYNONYMES")
            e = False
    return dictArg , e


def Main(taille, enc, path, e):
    pathRacine="../"
    fileDict="dictmots.csv"
    fileConc="listedeConc.csv"
    path=None
    path1="Textes/GerminalUTF8.txt"
    path2="Textes/LesTroisMousquetairesUTF8.txt"
    path3="Textes/LeVentreDeParisUTF8.txt"

    #   Modele
    calc = CoocurrencesCalculator(taille, enc, path1,path2,path3)
    #   Dao
    fc = FichierConnection()
    # stop words
    fc.aretirer()
    #   Vue
    gestionCMD=GestionCMD()
    #Files voulus
    e=True
    if e==True: #Entrainement
        #Calcul des coocurrences
        calc.setText()
        dictStop=fc.lectureDonneesDict('../stopword.csv')
        print("grandeur du dictionnaire stop : "+str(len(dictStop)))
        calc.setDict(dictStop)
        print("grandeur du dictionnaire : "+str(len(calc.dict)))
        calc.getCooc()
        fc.addToTableDict(calc.dict,pathRacine+fileDict)
        fc.verifDupliDict(calc.dict)
        fc.addToConcCSV(calc.dict, calc.matriceCooc, taille, pathRacine+fileConc)



if __name__ == "__main__":
    taille=3
    enc="utf-8"
    path="Textes/GerminalUTF8.txt"
    args , e = splitArgs()

    # Gestion des exception des argv
    if 't' in args:
        if type(args["t"])  == integer:
            print("Veuillez choisir un nombre comme taille après -t")
        else:
            taille = args["t"]
    if 'enc' in args:
        if args["enc"]=="utf-8" ==False:
            print("Veuillez choisir l'encodage utf-8 après -enc")
        else:
            enc=args["enc"]
    if 'cc' in args:
        if (args["cc"]).endswith(".txt") == False:
            print("Veuillez entrer un fichier texte après -cc")
        else:
            path=args["cc"]

    Main(taille, enc, path, e)

    #################
    #   TO TEST:    #
    #################

# LIGNE COMMANDE TRAINING
# python TP2.py -t 3 -e -enc utf-8 -cc Textes\GerminalUTF8.txt

# LIGNE COMMANDE SYNONYMES
# python TP2.py -t 3 -s
