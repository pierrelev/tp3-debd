import csv
import os.path
import time
import numpy as np
import csv as csv

class FichierConnection:
	def __init__(self):
		self

	def aretirer(self): # mots trop recurents a enlever du dictionnaire
		dicStop = {}
		enlevermot = ["alors", "au", "aucuns", "aussi", "autre", "avant", "avec", "avoir", "bon", "car", "ce", "cela", "ces",
					"ceux", "chaque", "ci", "comme", "comment", "dans", "des", "du", "dedans", "dehors", "depuis", "devrait",
					"doit", "donc", "dos", "début", "elle", "elles", "en", "encore", "essai" , "est", "et", "eu", "fait", "faites",
					"fois", "font", "hors", "ici", "il", "ils", "je", "à", "de", "l", "n", "y", "juste", "la", "le", "les", "leur",
					"là", "ma", "maintenant", "mais", "mes", "mine", "moins", "mon", "mot", "même", "ni", "nommés", "notre", "nous", "ou",
					"peut", "peu", "plupart", "pour", "pourquoi", "quand", "que", "quel", "quelle", "quelles", "quels", "qui", "sa", "sans",
					"ses", "seulement", "où", "par", "parce", "pas", "un", "une", "on", "si", "sien", "son", "sont", "sous", "soyez",
					"lequel","sujet", "sur", "ta", "tandis", "tellement", "tels", "tes", "ton", "tous", "tout", "trop", "très", "tu",
					'voient', "vont", "votre", "vous", "vu", "ça", "étaient", "état", "étions", "été", "être", ""]
		if  os.path.isfile('../stopword.csv') == False:
			for index, word in enumerate(enlevermot):
				dicStop[word] = index
			print("stop words dans le dictionnaire")
			self.addToTableDict(dicStop, '../stopword.csv')

	def addToTableDict(self, dict, file):
		with open(file, 'w', newline='') as csvFile:
			start = time.time()
			writer = csv.writer(csvFile)
			for each in dict:
				writer.writerow([dict[each],each])
		end = time.time()
		print("time fonction dictionnaire: ", end - start)

	def cleanMatrice(self, matrice):
		cleanMatrice = np.nonzero(matrice)
		cleanMatrice = np.transpose(cleanMatrice)
		return cleanMatrice

	def addToConcCSV(self, dict, matrice, taille, file):
		m = self.cleanMatrice(matrice)
		start = time.time()
		with open(file, 'a', newline='') as csvFile:
			writer = csv.writer(csvFile)
			for each in m:
				ligne = each[0]
				colonne = each[1]
				writer.writerow([ligne, colonne, matrice[ligne][colonne], taille])
		end = time.time()
		print("Remplir csv de coocurrence: ", end-start)

	def lectureDonneesDict(self, file):
		dictDonnees = {}
		with open(file, mode='r') as csv_file:
			csv_reader = csv.reader(csv_file)
			dictDonnees = {rows[0]:rows[1] for rows in csv_reader}
		return dictDonnees

	def nbreLignes(self,file):
		with open(file) as f:
			return sum(1 for line in f)

	def purgeStopwords(self, dict, stopwords):
		d = dict
		sw = stopwords
		for word in sw:
			d = {key:val for key, val in d.items() if val != word}
		return d

	def verifDupliDict(self, dict):
		new={}
		dNouveau = {}
		dAncien = {}
		start = time.time()
		nbreLignes=0

		if  os.path.isfile('../stopword.csv') == True:
			sw=self.lectureDonneesDict('../stopword.csv')
		if  os.path.isfile('../dictmots.csv') == True:
			dAncien=self.lectureDonneesDict('../dictmots.csv')
			
		dNouveau = self.purgeStopwords(dict, sw)
		
		for mot in dNouveau:
			if mot not in dAncien:
				new[mot]=nbreLignes
				nbreLignes+=1

		self.addToTableDict(new,'../dictmots.csv')
		end = time.time()
		print("time verif duplicates: ", end - start)

def Main():
	fich = FichierConnection()
	t = fich.lectureDonneesDict("Textes/test.txt")

if __name__ == "__main__":
	Main()