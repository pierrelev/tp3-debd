import numpy as np
import time
import re


class CoocurrencesCalculator:
    def __init__(self, taille, enc, path1, path2, path3):
        self.taille = taille
        self.enc = enc
        self.path1 = '../'+path1
        self.path2 = '../'+path2
        self.path3 = '../'+path3
        self.dict = {}
        self.text = None
        self.matriceCooc = None

    def getText(self):
        with open(self.path1, 'r', encoding=self.enc) as textFile:
            text1 = textFile.read()
            textFile.close()
            print("1. text"+str(len(text1)))
        
        with open(self.path2, 'r', encoding=self.enc) as textFile:
            text2=textFile.read()
            textFile.close()
            print("2. text"+str(len(text2)))

        with open(self.path3, 'r', encoding=self.enc) as textFile:
            text3=textFile.read()
            textFile.close()   
            print("3. text"+str(len(text3)))

        text=text1+" "+text2+" "+text3
        return text.lower()

        # Split une string et retourne Array de mots separees
    def splitWords(self, string):
        splitText = re.split("\W|\d", string)
        return splitText

        # Separe le texte par mots
    def getMotArray(self):
        t = self.getText()
        return self.splitWords(t)

    def setText(self):
        self.text = self.getMotArray()

    # Peuple le dictionnaire
    def setDict(self, dictStopwords):
        i = 0
        listeStop=list(dictStopwords.values())
        print("listeStop : "+str(listeStop))
        if len(self.text) >= 1:
            for word in self.text:
                if word not in self.dict and word not in listeStop:
                    self.dict[word] = i
                    i += 1

    # Helper pour trouver les index des mots
    def getIndexOf(self, word):
        index = None
        if word in self.dict:
            index = self.dict[word]
        else:
            index = "ERROR"
        return index

    # Ajouter 1 coocurrence dans la matrice
    def addCooc(self, wordA, wordB):
        if wordA in self.dict:
            iA = self.getIndexOf(wordA)
        else:
            return
        if wordB in self.dict:
            iB = self.getIndexOf(wordB)
        else:
            return

        # conserver seulement une moitiee
        if iA != "ERROR" and iB != "ERROR":
            if self.matriceCooc[iA][iB] == 0 and self.matriceCooc[iB][iA] == 0:
                self.matriceCooc[iA][iB] += 1
            elif self.matriceCooc[iA][iB] > 0:
                self.matriceCooc[iA][iB] += 1
            elif self.matriceCooc[iB][iA] > 0:
                self.matriceCooc[iB][iA] += 1

        # Peupler matrice de coocurrences
    def getCooc(self):
        self.matriceCooc = np.zeros((len(self.dict), len(self.dict)))
        coteFenetre = 0
        pair = False
        if self.taille % 2 == 0:
            pair = True  # Pair
            coteFenetre = self.taille//2+1

        else:
            pair = False  # impair
            coteFenetre = self.taille//2

        if pair is False:
            for i in range(coteFenetre, len(self.text)-coteFenetre):
                wordCentre = self.text[i]
                count = 1
                while(count <= coteFenetre):
                    wordGauche = self.text[i-(count)]
                    wordDroite = self.text[i+(count)]
                    self.addCooc(wordCentre, wordGauche)
                    self.addCooc(wordCentre, wordDroite)
                    count += 1
        elif pair is True:
            for i in range(coteFenetre, len(self.text)-coteFenetre+1):
                wordCentre = self.text[i]
                count = 1
                while(count < coteFenetre):
                    wordGauche = self.text[i-(count)]
                    wordDroite = self.text[i+(count)]
                    self.addCooc(wordCentre, wordGauche)
                    self.addCooc(wordCentre, wordDroite)
                    count += 1

    def rechercheS(self, word, nbSyn, taille, cnn):
        word = str(word)
        # iWord = cnn.getIndexDB(word)
        coocWord = None
        score = {}
        sortedScore = 0
        if iWord != "ERROR":
            # coocWord = cnn.getCooc(iWord, taille)
            # tabWords = cnn.getAllWords()
            for mot in tabWords:
                if not(mot == word):
                    # idx = cnn.getIndexDB(mot)
                    # row = cnn.getCooc(idx, taille)
                    # leastSquare
                    score[mot] = self.leastSquare(row, coocWord)
            sortedScore = sorted(score.items(), key=lambda kv: kv[1])[:nbSyn]
        else:
            sortedScore = "ERROR"
        return sortedScore

    def leastSquares(self, row, coocWord):
        subtract = np.subtract(row, coocWord)
        return np.sum(np.multiply(subtract, subtract))


def Main():
    taille=5
    enc="utf-8"
    path=None
    path1="Textes/GerminalUTF8.txt"
    path2="Textes/LesTroisMousquetairesUTF8.txt"
    path3="Textes/LeVentreDeParisUTF8.txt"
    cc = CoocurrencesCalculator(taille, enc, path1, path2, path3)


if __name__ == "__main__":
    Main()
