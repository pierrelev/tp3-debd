class GestionCMD:
    def __init__(self):
        self.input = None
        self.word = None
        self.nbSyn = None
        self.methode = None
        self.limitCommand = 3

    def writeInstructions(self):
        print(" \nEntrez un mot, le nombre de synonymes que vous voulez et la methode de calcul,")
        print("i.e. produit scalaire: 0, least squares: 1, cityblock: 2")
        print("\n \n Tapez -1 pour quitter \n ")

    def getInput(self):
        self.input = input()
        return self.input

    def error(self):
        print("\n    COMMANDE INVALIDE\n")

    def splitInput(self):
        vectInput = self.input.split()
        if len(vectInput) == self.limitCommand: #bon nombre d'arguments
            self.word = vectInput[0]
            if str.isdigit(vectInput[1]):
                self.nbSyn = int(vectInput[1])
            else:
                self.error()
                return
            if str.isdigit(vectInput[2]):
                self.methode = int(vectInput[2])
            else:
                self.error()
                return
            return self.word, self.nbSyn, self.methode

        else:
            self.error()
            return

    def printResultat(self, resultat):
            pass

def Main():
	gc = GestionCMD()

if __name__ == "__main__":
	Main()