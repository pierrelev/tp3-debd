#!/usr/bin/env python
# -*- coding: utf-8 -*-

#classe qui fait l'interation avec les dossier CSV
import csv

class ConnectionCSV:
    def __init__(self):
        self.X = []
        self.Y = []
        self.Z = []
        self.leDictionnaire = {}
        
    def lectureCSV(self, file,taille):
        dict={}
        i=0 
        listTuples=[]
        
        #Lire le fichier CSV voulu et le mettre dans listTuples

        with open(file) as f:
            for line in csv.reader(f):
                if int(line[3])==taille:
                    tuple=(int(line[0]),int(line[1]), round(float(line[2])),int(line[3]))
                    self.X.append(int(line[0]))
                    self.Y.append(int(line[1]))
                    self.Z.append(round(float(line[2])))
                    listTuples.append(tuple)
                    i=i+1
        # print(str(listTuples))
        return listTuples
    
    def lectureCSVdict(self, file):
        dictionnaire={}
        i=0 
        listTuples=[]
        
        #Lire le fichier CSV voulu et le mettre dans listTuples

        with open(file) as f:
            for line in csv.reader(f):
                dictionnaire[int(line[0])]=line[1]
                tuple=(line[0],line[1])
                listTuples.append(tuple)
                i=i+1
        self.leDictionnaire = dictionnaire
        return listTuples
    
    def getArrayValues(self):
        return self.X, self.Y, self.Z   
    