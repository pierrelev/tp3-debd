################################
##           TP3 BY           ##
##    -CAMILLE                ##
##    -PAOLA                  ##
##    -PIERRE                 ##
##    -WIL                    ##
################################

# -*- coding: utf-8 -*-

import sys
import time
from ConnectionCSV import *
from Cluster import *
class GestionCMD: #vue
    def __init__(self, t, nc, n):
        self.t = t
        self.nc = nc
        self.n = n
        
    def printIter(self, nbIter, nbChangements, dnc):
        print("Iteration ", nbIter, " terminee. ", nbChangements, " changements de clusters. \n")

        if (nbChangements > 0):
            for i, each in enumerate(dnc):
                print("Il y a ", len(dnc[each]) ," points (mots) regroupes autour du centroïde no ",i,".")
            print("\n============================================")

    def affichageResultat(self):
        print("Taille: ", self.t, " Nombre de cluster: ", self.nc,
        " Nombre de synonymes: ", self.n)

    def printGroupes(self):
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

    def printFinale(self, laMatrice, dictLSCluster, dictCluster, nbSynonymes, dictMots):
        for i, equipeIndex in dictCluster.items():
            equipeValues = laMatrice[equipeIndex]
            scores = dictLSCluster[i]
            equipeIndexValueScores = list(zip(equipeIndex,equipeValues, scores))

            print("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            print( "Groupe ",i, ": \n") 

            best = sorted(equipeIndexValueScores, key=lambda x: x[-1].all(), reverse=True)
            best = best[:nbSynonymes]

            for each in best:
                index = each[0]
                mot = dictMots[index]
                print(mot, " --> ", each[2])


#****************CONTROLEUR******************
def splitArgs():
    dictArg = {}
    e = True
    for i, value in enumerate(sys.argv):
        if(value == '-t'):
            dictArg["t"] =int(sys.argv[i+1])
            
        elif (value == '-nc'):
            dictArg["nc"] = int(sys.argv[i+1])
            
        elif (value == '-n'):
            dictArg["n"] = int(sys.argv[i+1])
            
    return dictArg

def Main(t, nbSynonymes, nbBarycentres):
    taille=t
    nSynonymes = nbSynonymes
    nBarycentres = nbBarycentres
    start=time.time()
    fileConc="../listedeConc.csv"
    fileDict="../dictmots.csv"
    g = GestionCMD(t, nc, n)
    g.affichageResultat()
    cluster = Clustering()
    connection = ConnectionCSV()
    connection.lectureCSV(fileConc,taille)
    connection.lectureCSVdict(fileDict)
    x, y, z= connection.getArrayValues()
    
    cluster.recreerMatrice(x, y, z)
    cluster.setBarycentres(nBarycentres)
    cluster.clusterIt()

    while len(cluster.dictOldCluster) <= 0:
        cluster.clusterIt()
        cluster.newBarycentres()
        iteration = 0
        changements = cluster.hasChanged()
        while True:
            cluster.clusterIt()
            lamatrice = cluster.newBarycentres()
            changements, dictNewCluster = cluster.hasChanged()
            if changements[0] == False:
                g.printIter(iteration,changements[1], dictNewCluster)
                break
            else:
                g.printIter(iteration,changements[1], dictNewCluster)
            iteration = iteration + 1
    
    g.printGroupes()
    
    end = time.time()
    g.printFinale(lamatrice, cluster.dictLSCluster, cluster.dictNewCluster, nSynonymes, connection.leDictionnaire)

if __name__=="__main__":
    t=0     #taille
    nc=0	#nbre de cluster
    n=0 	#nbre de synonymes
    args= splitArgs()

#Gestion des exceptions des arguments
    if 't' in args:
        if args["t"].__class__ is not int:
            print("Veuillez choisir un nombre comme taille de fenetre")
        else:
            t =args["t"]

    if 'nc' in args:
        if args["nc"].__class__ is not int:
            print("Veuillez choisir un nombre comme nombre de cluster")
        else:
            nc=args["nc"]

    if 'n' in args:
        if args["n"].__class__ is not int:
            print("Veuillez choisir un nombre comme nombre de synonymes")
        else:
            n=args["n"]

    Main(t , n , nc)        
