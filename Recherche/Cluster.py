#!/usr/bin/env python
# -*- coding: utf-8 -*-
################################
##           TP3 BY           ##
##    -CAMILLE                ##
##    -PAOLA                  ##
##    -PIERRE                 ##
##    -WIL                    ##
################################
from random import randint
import random
import numpy as np

class Clustering:
    def __init__(self):
        self.dictLSCluster = {}
        self.dictCooc={}
        self.barycentres=[]
        self.dictOldCluster={}
        self.dictNewCluster={}
        self.nbBarycentres=3

        self.X = []
        self.Y = []
        self.Z = []
        self.laMatrice = None

    def listeSelonTailleVoulue(self,listeTuples,taille):
        nouvelleListe=[t for t in listeTuples if t[3]==taille]
        return nouvelleListe
    
    def recreerMatrice(self, x, y, z):
        xy = np.amax(x) + 5

        self.laMatrice = np.zeros([xy,xy])

        self.laMatrice[y,x] = z
        self.laMatrice[x,y] = z
         
    def hasChanged(self):
        if len(self.dictOldCluster) > 0:
            different = True
            diffkeys = []
            for i , each in enumerate(self.dictOldCluster):
                if self.dictOldCluster[i] != self.dictNewCluster[i]:
                    diffkeys.append(i)
            if(len(diffkeys) == 0):
                different = False
            diffLen = []
            for k in diffkeys:
                d = len(self.dictNewCluster[k]) - len(self.dictOldCluster[k])
                if d > 0:
                    diffLen.append(d)
            return [different , sum(diffLen)], self.dictNewCluster
        
    def setBarycentres(self, limit):
        barycentres = []
        while len(barycentres) < limit:
            b = random.choice(self.laMatrice)
            b = np.average(b)
            while b in barycentres:
                b = random.choice(self.laMatrice)
                b = np.average(b)
            barycentres.append(b)
        self.barycentres = barycentres
        
    def leastSquares(self, row, barycentre):
        subtract = np.subtract(row, barycentre)
        ls = np.sum(np.multiply(subtract, subtract))
        return ls

    def distanceToBarycentres(self, row):
        distances  = []
        for j , value in enumerate(self.barycentres):
            distances.append(self.leastSquares(row, value))
        # print(distances)
        return distances

    def getMinIndexOnArray(self, array):
        return array.index(min(array))

    def getCleanDictCluster(self):
        clusters = {}
        for i , key in enumerate(self.barycentres):
            clusters[i] = []
        return clusters

    def clusterIt(self):
        self.dictOldCluster = self.dictNewCluster
        clusters = self.getCleanDictCluster()
        dictLSCluster = self.getCleanDictCluster()

        for i, row in enumerate(self.laMatrice):
            self.printLoading(i)
            distances = self.distanceToBarycentres(row)
            capitaine = self.getMinIndexOnArray(distances)
            dictLSCluster[capitaine].append(distances[capitaine])
            clusters[capitaine].append(i)
            self.printLoading(i)
        self.dictLSCluster = dictLSCluster
        self.dictNewCluster = clusters
        return clusters
        
    def getValueArray(self, keyArray):
        vArray = []
        for each in keyArray:
            vArray.append(self.dictCooc[each])
        return vArray

    def newBarycentres(self):
        newBarycentres = []
        for key , value in self.dictNewCluster.items():
            equipe = self.laMatrice[value]
            newBarycentreI = np.average(equipe)
            newBarycentres.append(newBarycentreI)
        self.barycentres = newBarycentres
        return self.laMatrice
    
    def printLoading(self, i):
        num_string = str(i)
        dic = {'0':'    >','1':'    >>','2':'    >>>','3':'    >>>>','4':'    >>>>>','5':'    >>>>>>','6':'    >>>>>>>','7':'    >>>>>>>>','8':'    >>>>>>>>>','9':'    >>>>>>>>>>'}#, '10': '    >>>>>>>>>>>>>>>>>', '11': '    >>>>>>>>>>>>>>>>>>','12': '    >>>>>>>>>>>>>>>>>>>', '13': '    >>>>>>>>>>>>>>>>>>>>', '14': '    >>>>>>>>>>>>>>>>>>>>>', '15': '    >>>>>>>>>>>>>>>>>>>>>>', '16': '    >>>>>>>>>>>>>>>>>>>>>>>', '17': '    >>>>>>>>>>>>>>>>>>>>>>>>', '18': '    >>>>>>>>>>>>>>>>>>>>>>>>>', '19': '    >>>>>>>>>>>>>>>>>>>>>>>>>>'}
        
        if i > 10000: #100000
            toTest = num_string[-3] #six ideal
            if (num_string[-3:] == '000'):
                print ("                                   ", end="\r")

            if toTest in dic:
                print (dic[toTest], end="\r")
